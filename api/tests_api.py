from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase
from api.models import Supply


@freeze_time("2017-07-1 22:24:57", tz_offset=-4)
class SupplyViewSetTestCase(APITestCase):

    URL_BASE = 'http://localhost:8000/api/consumo-gasolina/supply/'

    def setUp(self):
        Supply.objects.create(km_supply="10000",
                              quantity_liters=44.56,
                              value=133.24,
                              date_supply="2017-07-1 22:24:57.891964+00:00",
                              created_at="2017-07-1 22:24:57.891964+00:00")

        Supply.objects.create(km_supply="10560",
                              quantity_liters=44.56,
                              value=133.24,
                              date_supply="2017-07-14 22:24:57.891964+00:00",
                              created_at="2017-07-14 22:24:57.891964+00:00")

    def test_sum_supply(self):
        data = {'quantity_liters': 89.12}
        response = self.client.get(self.URL_BASE + 'sum_supply/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)

    def test_count_supply(self):
        data = {'quantity_supply': 2}
        response = self.client.get(self.URL_BASE + 'count_supply/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)

    def test_diff_km_supply(self):
        data = {'quantity_km': 560}
        response = self.client.get(self.URL_BASE + 'diff_km_supply/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)

    def test_km_liters(self):
        data = {'km_liters': 6.283662477558348}
        response = self.client.get(self.URL_BASE + 'calculate_km_liters/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, data)
